@extends('tamplate')
@section('title','Home')
@section('content')
<!-- Page Header-->
        <header class="masthead" style="background-image: url('assets/img/home.jpg')">
            <div class="container position-relative px-4 px-lg-5">
                <div class="row gx-4 gx-lg-5 justify-content-center">
                    <div class="col-md-10 col-lg-8 col-xl-7">
                        <div class="site-heading">
                            <h1>MASRURI</h1>
                            <span class="subheading">Web Developer and Creator</span>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- Main Content-->
        <div class="container px-4 px-lg-5">
            <div class="row gx-4 gx-lg-5 justify-content-center">
                <div class="col-md-10 col-lg-8 col-xl-7">
                            <h2 class="post-title, justify-content-center">Wise Words</h2>
                            <br class="post-subtitle">* Waktumu terbatas, jangan habiskan untuk hidup orang lain. - Steve Jobs </br>
                            <br class="post-subtitle">* Jangan tunda pekerjaanmua sampai besok, sementara kau bisa mengerjakannya hari ini. - Benjamin Franklin</br>
                            <br class="post-subtitle">* Tetapkan tujuan Anda tinggi-tinggi dan jangan berhenti sampai Anda mencapainya. - Bo Jakcson</br>
                            <br class="post-subtitle">* Kesempatan itu mirip seperti matahari terbit, kalau kau menunggu terlalu lama, kau bisa melewatkannya. - William Arthur Ward</br>
                        </a>
                    </div>
                    <!-- Divider-->
                    <hr class="my-4" />
                </div>
            </div>
        </div>
@endsection

