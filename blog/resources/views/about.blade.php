@extends('tamplate')
@section('title','About')
@section('content')
<!-- Page Header-->
        <header class="masthead" style="background-image: url('assets/img/about.jpg')">
            <div class="container position-relative px-4 px-lg-5">
                <div class="row gx-4 gx-lg-5 justify-content-center">
                    <div class="col-md-10 col-lg-8 col-xl-7">
                        <div class="page-heading">
                            <h1>It's Me</h1>
                            <span class="subheading">This is about me</span>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- Main Content-->
        <main class="mb-4">
            <div class="container px-4 px-lg-5">
                <div class="row gx-4 gx-lg-5 justify-content-center">
                    <div class="col-md-10 col-lg-8 col-xl-7">
                        <br>Hello... Let me introduce my self,</br>
                        <br>My name is Masruri, people ussually call me Rury. I'm from Jakarta. I really like music and read book, I am also interested in develope website and video creator. I used to work as Corporate Secretary at PT MNC Sky Vision Tbk and PT MNC Vision Network Tbk. Now I work as Human Resource Development at PT Satria Antaran Prima Tbk.</br>
                        <br>That's all from me, thank you for your attention :)</br>
                    </div>
                </div>
            </div>
        </main>
@endsection